﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2CreditCards
{
    class Card
    {
        private static void Main()
        {
            //Objects creation
            CreditCard Card1 = new CreditCard("111 2222 3333 4444", 2000);
            CreditCard Card2 = new CreditCard("5555 6666 7777 8888", 3000);
            CreditCard Card3 = new CreditCard("9999 8888 7777 6666", 1000);

            //Make a deposit
            Card1.Deposit(1000);
            Card2.Deposit(1000);

            //Make a withdraw
            Card3.Withdraw(300);

            //Display current balance of cards
            Card1.DisplayBalance();
            Card2.DisplayBalance();
            Card3.DisplayBalance();
        }
    }
}
