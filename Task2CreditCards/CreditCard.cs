﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Task2CreditCards
{
    class CreditCard
    {
        public string AccNumber { get; private set; }
        public decimal CurrBalance { get; private set; }

        public CreditCard(string accountNumber, decimal initialBalance)
        {
            AccNumber = accountNumber;
            CurrBalance = initialBalance;
        }
        //Making a deposit method
        public void Deposit(decimal amount)
        {
            if (amount > 0)
            {
                CurrBalance += amount;
            }
            else
            {
                Console.WriteLine("Wrong amount");
            }
        }
        //Making a withdraw method
        public void Withdraw(decimal amount)
        {
            if (amount > 0 && amount <= CurrBalance)
            {
                CurrBalance -= amount;
            }
            else
            {
                Console.WriteLine("Balance is negative");
            }
        }
        //Displaying of a current card balance method
        public void DisplayBalance()
        {
            Console.WriteLine($"Account Number: {AccNumber}, Current account balance: {CurrBalance}");
        }
    }
}
