﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Task1Phone
{
    class Phone
    {
        public string Number { get; set; }
        public string Model { get; set; }
        public int Weight { get; set; }

        public Phone(string number, string model, double weight)
        {
            Number = number;
            Model = model;  
            Weight = (int)weight;
        }

        public void ReceiveCall(string AbonentName)
        {
            Console.WriteLine($"{AbonentName} is calling"); 
        }

        public string GetNumber()
        {
            return Number;
        }

        public void SendMessage(params string[] phoneNumbers)
        {

            Console.WriteLine("Send message to these numbers: ");
            foreach (var phoneNumber in phoneNumbers)
            {
                Console.WriteLine(phoneNumber);
            }
        }
    }
}

