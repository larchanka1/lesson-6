﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1Phone
{
    class Program
    {
        static void Main()
        {
            //1st phone instance
            Phone phone1 = new Phone("111-222-3333", "iPhone 13", 200);
            Phone phone2 = new Phone("444-555-6666", "Samsung Galaxy S9", 210);
            Phone phone3 = new Phone("777-888-9999","Nokia 7310", 220);

            //Display the parameters of phones to console
            Console.WriteLine("Phone 1:");
            Console.WriteLine("Number: " + phone1.Number);
            Console.WriteLine("Model: " + phone1.Model);
            Console.WriteLine("Weight" + phone1.Weight);

            Console.WriteLine("Phone 2:");
            Console.WriteLine("Number: " + phone2.Number);
            Console.WriteLine("Model: " + phone2.Model);
            Console.WriteLine("Weight" + phone2.Weight);

            Console.WriteLine("Phone 3:");
            Console.WriteLine("Number: " + phone3.Number);
            Console.WriteLine("Model: " + phone3.Model);
            Console.WriteLine("Weight: " + phone3.Weight);

            Console.ReadLine();

            phone1.ReceiveCall("Nika");
            Console.WriteLine($"Phone 1 Number: {phone1.GetNumber()}");
            phone1.SendMessage("937-99-92");

            phone2.ReceiveCall("Alex");
            Console.WriteLine($"Phone 2 Number: {phone2.GetNumber()}");
            phone2.SendMessage("789-78-89", "123-78-09");

            phone3.ReceiveCall("Jack");
            Console.WriteLine($"Phone 3 Number: {phone3.GetNumber()}");
            phone3.SendMessage("000-14-17");

            Console.ReadLine();
        }
    }
}
